#!/usr/bin/python
# -*- coding: iso-8859-4 -*-
import sys
import time

current_micro_time = lambda: int(round(time.time() * 1000 * 1000))

start = current_micro_time()

result = ""
if len(sys.argv) != 3:
	print "%s <dictionary> <word>" % sys.argv[0]
	sys.exit(1)

lz = len(sys.argv[2])
l = sorted(sys.argv[2])

data = open(sys.argv[1], "r").read()
txt = data.split("\r\n")

for r in txt:
	if lz == len(r):
		if sorted(r) == l:
			result += ","+r

print str(current_micro_time()-start) + result

